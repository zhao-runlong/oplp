package com.zrl.project.service.impl;

import com.zrl.project.model.VO.commentVO.CommentsInfoVO;
import com.zrl.project.service.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 13:32
 * @description:
 */
@SpringBootTest
class CommentServiceImplTest {

    @Resource
    private CommentService commentService;

    @Test
    void getComments() {
        List<CommentsInfoVO> commentsByBlogId = commentService.getCommentsByBlogId(10L);
        System.out.println(commentsByBlogId);
    }
}