package com.zrl.project.service;

import com.zrl.project.model.entity.QuestionLike;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zrl.project.model.entity.User;
import com.zrl.project.model.request.QuestionListLikeStatusRequest;

import java.util.List;

/**
* @author 13168
* @description 针对表【question_like】的数据库操作Service
* @createDate 2023-04-02 02:54:02
*/
public interface QuestionLikeService extends IService<QuestionLike> {

    Integer modifyLikeStatus(QuestionLike questionLike);

    Boolean getQuestionLikeStatus(QuestionLike questionLike);

    List<User> getLikeQuestionUsers(Long id);

    List<Boolean> getQuestionListLikeStatus(QuestionListLikeStatusRequest list);
}
