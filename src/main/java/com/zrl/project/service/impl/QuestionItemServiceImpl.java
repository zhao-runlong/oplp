package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.QuestionItem;
import com.zrl.project.service.QuestionItemService;
import com.zrl.project.mapper.QuestionItemMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【question_item(题目选项表)】的数据库操作Service实现
* @createDate 2023-04-01 16:32:00
*/
@Service
public class QuestionItemServiceImpl extends ServiceImpl<QuestionItemMapper, QuestionItem>
    implements QuestionItemService{

}




