package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.PaperQuestion;
import com.zrl.project.service.PaperQuestionService;
import com.zrl.project.mapper.PaperQuestionMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【paper_question】的数据库操作Service实现
* @createDate 2023-04-06 20:36:24
*/
@Service
public class PaperQuestionServiceImpl extends ServiceImpl<PaperQuestionMapper, PaperQuestion>
    implements PaperQuestionService{

}




