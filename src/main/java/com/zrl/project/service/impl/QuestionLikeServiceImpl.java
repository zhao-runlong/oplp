package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.entity.Likes;
import com.zrl.project.model.entity.QuestionLike;
import com.zrl.project.model.entity.User;
import com.zrl.project.model.request.QuestionListLikeStatusRequest;
import com.zrl.project.service.QuestionLikeService;
import com.zrl.project.mapper.QuestionLikeMapper;
import com.zrl.project.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 13168
* @description 针对表【question_like】的数据库操作Service实现
* @createDate 2023-04-02 02:54:02
*/
@Service
public class QuestionLikeServiceImpl extends ServiceImpl<QuestionLikeMapper, QuestionLike>
    implements QuestionLikeService{

    @Resource
    private UserService userService;

    /**
     * 查询是否存在指定Like对象
     * @param questionLike
     * @return
     */
    private QuestionLike getExist(QuestionLike questionLike) {
        LambdaQueryWrapper<QuestionLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QuestionLike::getQuestionId, questionLike.getQuestionId());
        queryWrapper.eq(QuestionLike::getUserId, questionLike.getUserId());
        return getOne(queryWrapper);
    }



    @Override
    public Integer modifyLikeStatus(QuestionLike questionLike) {
        QuestionLike one = getExist(questionLike);
        if (one == null){
            boolean save = save(questionLike);
            if(!save){
                throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
            }
            return 1;
        }
        LambdaQueryWrapper<QuestionLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QuestionLike::getQuestionId,questionLike.getQuestionId());
        queryWrapper.eq(QuestionLike::getUserId,questionLike.getUserId());
        boolean remove = remove(queryWrapper);
        if (!remove){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"取消点赞失败");
        }
        return 0;
    }

    @Override
    public Boolean getQuestionLikeStatus(QuestionLike questionLike) {
        QuestionLike one = getExist(questionLike);
        if (one == null){
            return false;
        }
        return true;
    }

    @Override
    public List<User> getLikeQuestionUsers(Long id) {
        LambdaQueryWrapper<QuestionLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QuestionLike::getQuestionId,id);
        List<QuestionLike> list = list(queryWrapper);

        if (list.isEmpty()){
            return new ArrayList<>();
        }

        List<Long> userIdList = list.stream()
                .map(QuestionLike::getUserId)
                .collect(Collectors.toList());
        return userService.listByIds(userIdList);
    }


    @Override
    public List<Boolean> getQuestionListLikeStatus(QuestionListLikeStatusRequest list) {
        List<Long> questionIds = list.getQuestionIds();
        Long userId = list.getUserId();
        return questionIds.stream().map(id -> {
            LambdaQueryWrapper<QuestionLike> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(QuestionLike::getQuestionId, id);
            queryWrapper.eq(QuestionLike::getUserId, userId);
            return getOne(queryWrapper) != null;
        }).collect(Collectors.toList());
    }
}




