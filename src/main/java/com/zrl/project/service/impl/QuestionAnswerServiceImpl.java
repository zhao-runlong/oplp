package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.QuestionAnswer;
import com.zrl.project.service.QuestionAnswerService;
import com.zrl.project.mapper.QuestionAnswerMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【question_answer(题目答案表)】的数据库操作Service实现
* @createDate 2023-04-01 16:31:56
*/
@Service
public class QuestionAnswerServiceImpl extends ServiceImpl<QuestionAnswerMapper, QuestionAnswer>
    implements QuestionAnswerService{

}




