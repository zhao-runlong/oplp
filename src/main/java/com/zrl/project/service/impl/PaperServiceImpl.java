package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.VO.paperVO.AddPaperParamVO;
import com.zrl.project.model.VO.paperVO.PaperDetailVO;
import com.zrl.project.model.VO.paperVO.PaperWithUserInfo;
import com.zrl.project.model.VO.qustionVO.QuestionDetailVO;
import com.zrl.project.model.entity.*;
import com.zrl.project.service.*;
import com.zrl.project.mapper.PaperMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 13168
* @description 针对表【paper(试卷表)】的数据库操作Service实现
* @createDate 2023-04-07 00:45:31
*/
@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper, Paper>
    implements PaperService{

    
    @Resource
    private PaperQuestionService paperQuestionService;
    
    @Resource
    private PaperTagsService paperTagsService;

    @Resource
    private UserService userService;

    @Resource
    private TagsService tagsService;

    @Resource
    private QuestionService questionService;
    
    
    @Override
    @Transactional
    public Boolean addPaper(AddPaperParamVO addPaperParamVO) {
        List<Long> questionIds = addPaperParamVO.getQuestionIds();
        if (questionIds.isEmpty() || questionIds.size() < 3){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"选择的题目数量不能小于三道");
        }
        //加入试卷基本信息
        Paper paper = new Paper();
        BeanUtils.copyProperties(addPaperParamVO,paper);

        boolean save = save(paper);
        if (!save){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"添加失败");
        }
        //获得保存后的试卷id
        Integer id = paper.getId();

        //保存试卷标签
        List<Integer> tags = addPaperParamVO.getTags();
        List<PaperTags> tagsList = tags.stream().map(tag -> {
            PaperTags paperTags = new PaperTags();
            paperTags.setPaperId(id);
            paperTags.setTagsId(tag);
            return paperTags;
        }).collect(Collectors.toList());
        boolean b = paperTagsService.saveBatch(tagsList);
        if (!b){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"上传题目失败");
        }

        //保存试卷题目

        List<PaperQuestion> paperQuestionList = questionIds.stream().map(questionId -> {
            PaperQuestion paperQuestion = new PaperQuestion();
            paperQuestion.setPaperId(id);
            paperQuestion.setQuestionId(questionId);
            return paperQuestion;
        }).collect(Collectors.toList());

        return paperQuestionService.saveBatch(paperQuestionList);
    }

    @Override
    public List<PaperWithUserInfo> queryPapers(AddPaperParamVO paper) {
        LambdaQueryWrapper<Paper> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Paper::getCreateTime);

        if (paper == null) {

            return list(queryWrapper)
                    .stream()
                    .map(this::getPaperWithUserInfo)
                    .collect(Collectors.toList());
        }

        Long userId = paper.getUserId();
        //搜索指定用户的题目
        if (paper.getUserId() != null){
            queryWrapper.eq(Paper::getUserId,userId);
        }

        //通过标题内容查询试卷
        String title = paper.getTitle();
        if (StringUtils.isNotBlank(title)) {
            //查询内容和描述与输入内容相匹配的question
            queryWrapper
                    .like(Paper::getTitle, title)
                    .or()
                    .like(Paper::getDescription,title);
        }

        //通过标签查询
        List<Integer> tags = paper.getTags();
        if (tags!=null && !tags.isEmpty()){
            List<List<Integer>> paperIdList = new ArrayList<>();
            tags.forEach((tagId)->{
                LambdaQueryWrapper<PaperTags> paperTagsLambdaQueryWrapper = new LambdaQueryWrapper<>();
                paperTagsLambdaQueryWrapper.eq(PaperTags::getTagsId,tagId);
                List<Integer> paperId1List = paperTagsService.list(paperTagsLambdaQueryWrapper)
                        .stream().map(PaperTags::getPaperId).collect(Collectors.toList());
                paperIdList.add(paperId1List);
            });

            List<Integer> paperIds = paperIdList.get(0);
            paperIdList.forEach(paperIds::retainAll);

            if (paperIds.isEmpty()){
                queryWrapper.eq(Paper::getId,-1);
            }else {
                paperIds.forEach(id->{
                    queryWrapper.eq(Paper::getId,id);
                    if (paperIdList.indexOf(id) != paperIdList.size()){
                        queryWrapper.or();
                    }
                });
            }
        }

        return list(queryWrapper)
                .stream()
                .map(this::getPaperWithUserInfo)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean deleteById(Integer id) {
        //删除试卷标签表
        LambdaQueryWrapper<PaperTags> paperTagsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        paperTagsLambdaQueryWrapper.eq(PaperTags::getPaperId,id);
        boolean remove = paperTagsService.remove(paperTagsLambdaQueryWrapper);
        if (!remove){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"删除试卷标签失败");
        }
        //删除试卷题目表
        LambdaQueryWrapper<PaperQuestion> paperQuestionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        paperQuestionLambdaQueryWrapper.eq(PaperQuestion::getPaperId,id);
        boolean remove1 = paperQuestionService.remove(paperQuestionLambdaQueryWrapper);
        if (!remove1){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"删除试卷题目失败");
        }

        //删除试卷
        return removeById(id);
    }

    @Override
    @Transactional
    public PaperDetailVO queryPaperDetail(Integer paperId) {
        if (paperId == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        PaperDetailVO paperDetailVO = new PaperDetailVO();

        //查询基本信息
        Paper paper = getById(paperId);

        //查询标签信息和用户信息
        PaperWithUserInfo paperWithUserInfo = getPaperWithUserInfo(paper);
        BeanUtils.copyProperties(paper,paperWithUserInfo);

        //查询题目id列表
        LambdaQueryWrapper<PaperQuestion> questionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        questionLambdaQueryWrapper.eq(PaperQuestion::getPaperId,paperId);
        List<PaperQuestion> list = paperQuestionService.list(questionLambdaQueryWrapper);
        List<Long> questionIds = list.stream().map(PaperQuestion::getQuestionId).collect(Collectors.toList());

        //查询题目项
        List<QuestionDetailVO> questionDetailVOList = questionIds.stream().map(questionId -> {
            return questionService.getQuestionDetail(questionId);
        }).collect(Collectors.toList());

        BeanUtils.copyProperties(paperWithUserInfo,paperDetailVO);
        paperDetailVO.setQuestions(questionDetailVOList);

        return paperDetailVO;
    }

    @Override
    public AddPaperParamVO getUpdatePaper(Integer paperId) {
        if (paperId == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        AddPaperParamVO addPaperParamVO = new AddPaperParamVO();

        Paper paper = getById(paperId);
        BeanUtils.copyProperties(paper,addPaperParamVO);

        //设置标签
        LambdaQueryWrapper<PaperTags> tagsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        tagsLambdaQueryWrapper.eq(PaperTags::getPaperId, paper.getId());
        List<PaperTags> list = paperTagsService.list(tagsLambdaQueryWrapper);
        List<Integer> tagsList = list.stream().map(PaperTags::getTagsId).collect(Collectors.toList());
        addPaperParamVO.setTags(tagsList);

        //查询题目id列表
        LambdaQueryWrapper<PaperQuestion> questionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        questionLambdaQueryWrapper.eq(PaperQuestion::getPaperId,paperId);
        List<PaperQuestion> paperQuestionList = paperQuestionService.list(questionLambdaQueryWrapper);
        List<Long> questionIds = paperQuestionList.stream().map(PaperQuestion::getQuestionId).collect(Collectors.toList());
        addPaperParamVO.setQuestionIds(questionIds);

        return addPaperParamVO;
    }

    @Override
    @Transactional
    public Boolean updatePaper(AddPaperParamVO addPaperParamVO) {
        List<Long> questionIds = addPaperParamVO.getQuestionIds();
        if (questionIds.isEmpty() || questionIds.size() < 3){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"选择的题目数量不能小于三道");
        }
        //加入试卷基本信息
        Paper paper = new Paper();
        BeanUtils.copyProperties(addPaperParamVO,paper);

        boolean save = updateById(paper);
        if (!save){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"添加失败");
        }

        //获得保存后的试卷id
        Integer id = paper.getId();

        //保存试卷标签
        List<Integer> tags = addPaperParamVO.getTags();
        List<PaperTags> tagsList = tags.stream().map(tag -> {
            PaperTags paperTags = new PaperTags();
            paperTags.setPaperId(id);
            paperTags.setTagsId(tag);
            return paperTags;
        }).collect(Collectors.toList());
        LambdaQueryWrapper<PaperTags> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PaperTags::getPaperId,id);
        //删除之前存的标签
        paperTagsService.remove(queryWrapper);
        boolean b = paperTagsService.saveBatch(tagsList);
        if (!b){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"修改题目失败");
        }

        //修改试卷题目
        List<PaperQuestion> paperQuestionList = questionIds.stream().map(questionId -> {
            PaperQuestion paperQuestion = new PaperQuestion();
            paperQuestion.setPaperId(id);
            paperQuestion.setQuestionId(questionId);
            return paperQuestion;
        }).collect(Collectors.toList());

        LambdaQueryWrapper<PaperQuestion> paperQuestionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        paperQuestionLambdaQueryWrapper.eq(PaperQuestion::getPaperId,id);
        paperQuestionService.remove(paperQuestionLambdaQueryWrapper);

        return paperQuestionService.saveBatch(paperQuestionList);
    }


    /**
     * 查询试卷用户信息
     * @param paper
     * @return
     */
    public PaperWithUserInfo getPaperWithUserInfo(Paper paper) {

        List<Tags> tags = getTagsList(paper);
        List<String> stringList =
                tags.stream().map(Tags::getContent).collect(Collectors.toList());

        //根据用户id查询用户信息
        Long userId = paper.getUserId();
        User user = userService.getById(userId);
        //将用户名称和用户头像,评论数量，点赞数量，题目标签加入到QuestionWithUserInfoVO中
        PaperWithUserInfo paperWithUserInfo = new PaperWithUserInfo();
        BeanUtils.copyProperties(paper,paperWithUserInfo);
        paperWithUserInfo.setUserName(user.getUserName());
        paperWithUserInfo.setUserAvatar(user.getUserAvatar());
        paperWithUserInfo.setTags(stringList);
        return paperWithUserInfo;
    }


    /**
     * 查询试卷标签
     * @param paper
     * @return
     */
    private List<Tags> getTagsList(Paper paper) {
        LambdaQueryWrapper<PaperTags> tagsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        tagsLambdaQueryWrapper.eq(PaperTags::getPaperId, paper.getId());
        List<PaperTags> list = paperTagsService.list(tagsLambdaQueryWrapper);
        List<Integer> tagsList = list.stream().map(PaperTags::getTagsId).collect(Collectors.toList());

        return tagsService.listByIds(tagsList);
    }
}




