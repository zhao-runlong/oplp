package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.PaperTags;
import com.zrl.project.service.PaperTagsService;
import com.zrl.project.mapper.PaperTagsMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【paper_tags】的数据库操作Service实现
* @createDate 2023-04-07 00:45:27
*/
@Service
public class PaperTagsServiceImpl extends ServiceImpl<PaperTagsMapper, PaperTags>
    implements PaperTagsService{

}




