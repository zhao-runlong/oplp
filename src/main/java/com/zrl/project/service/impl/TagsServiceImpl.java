package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.Tags;
import com.zrl.project.service.TagsService;
import com.zrl.project.mapper.TagsMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【tags(标签表)】的数据库操作Service实现
* @createDate 2023-03-31 13:35:39
*/
@Service
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tags>
    implements TagsService{

}




