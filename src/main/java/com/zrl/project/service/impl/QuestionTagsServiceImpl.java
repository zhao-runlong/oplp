package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.entity.QuestionTags;
import com.zrl.project.service.QuestionTagsService;
import com.zrl.project.mapper.QuestionTagsMapper;
import org.springframework.stereotype.Service;

/**
* @author 13168
* @description 针对表【question_tags(题目标签表)】的数据库操作Service实现
* @createDate 2023-03-31 13:41:36
*/
@Service
public class QuestionTagsServiceImpl extends ServiceImpl<QuestionTagsMapper, QuestionTags>
    implements QuestionTagsService{

}




