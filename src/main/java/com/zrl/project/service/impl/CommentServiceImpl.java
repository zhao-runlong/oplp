package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zrl.project.model.VO.commentVO.CommentAndUserInfoVO;
import com.zrl.project.model.VO.commentVO.CommentsInfoVO;
import com.zrl.project.model.entity.Comment;
import com.zrl.project.model.entity.User;
import com.zrl.project.service.CommentService;
import com.zrl.project.mapper.CommentMapper;
import com.zrl.project.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 13168
* @description 针对表【comment(评论信息表)】的数据库操作Service实现
* @createDate 2023-03-26 00:01:21
*/
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>
    implements CommentService{

    @Resource
    private UserService userService;

    @Override
    public List<CommentsInfoVO> getCommentsByBlogId(Long blogId) {

        //查询所有对博客的评论
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getBlogId,blogId);
        return getCommentsInfoVOS(queryWrapper);
    }

    @Override
    public Boolean modifyTopReplyCommentId(Long id) {

        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getId,id);
        Comment comment = getOne(queryWrapper);
        comment.setTopCommentId(id);
        return updateById(comment);
    }

    @Override
    public Boolean deleteCommentById(Long id) {
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getId,id)
                .or()
                .eq(Comment::getReplyCommentId,id);
        return remove(queryWrapper);
    }

    @Override
    public List<CommentsInfoVO> getCommentsByQuestionId(Long id) {
        //查询所有对该题目的评论
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getQuestionId,id);
        return getCommentsInfoVOS(queryWrapper);
    }

    @NotNull
    private List<CommentsInfoVO> getCommentsInfoVOS(LambdaQueryWrapper<Comment> queryWrapper) {
        queryWrapper.orderByDesc(Comment::getCommentType);
        queryWrapper.orderByAsc(Comment::getCreateTime);
        List<Comment> AllList = list(queryWrapper);

        List<Comment> topCommentList = new ArrayList<>();

        AllList.stream().peek((item) -> {
            if (item.getTopCommentId() == item.getId()) {
                topCommentList.add(item);
            }
        }).collect(Collectors.toList());


        //转为所需对象，并查询用户设置头像和用户名,并设置子评论
        List<CommentsInfoVO> commentsInfoVOList = topCommentList.stream().map((comment) -> {

            CommentsInfoVO commentsInfoVO = new CommentsInfoVO();
            BeanUtils.copyProperties(comment, commentsInfoVO);
            User user = userService.getById(comment.getUserId());
            commentsInfoVO.setUserName(user.getUserName());
            commentsInfoVO.setUserAvatar(user.getUserAvatar());

            //查询所有顶层评论为当前评论的评论信息
            LambdaQueryWrapper<Comment> commentsQueryWrapper = new LambdaQueryWrapper<>();
            commentsQueryWrapper.eq(Comment::getTopCommentId, commentsInfoVO.getId());
            commentsQueryWrapper.ne(Comment::getId, commentsInfoVO.getTopCommentId());
            commentsQueryWrapper.orderByAsc(Comment::getCreateTime);
            List<Comment> commentList = list(commentsQueryWrapper);

            //将查询到的信息添加到CommentsInfoVO中
            List<CommentAndUserInfoVO> commentAndUserInfoVOList = commentList.stream().map(item -> {
                CommentAndUserInfoVO commentAndUserInfoVO = new CommentAndUserInfoVO();
                BeanUtils.copyProperties(item, commentAndUserInfoVO);
                //查此评论所属人的用户信息
                User otherUser = userService.getById(item.getUserId());
                commentAndUserInfoVO.setUserName(otherUser.getUserName());
                commentAndUserInfoVO.setUserAvatar(otherUser.getUserAvatar());
                //查询要回复人的用户信息
                User replyUser = userService.getById(item.getReplyUserId());
                commentAndUserInfoVO.setReplyUserName(replyUser.getUserName());
                return commentAndUserInfoVO;
            }).collect(Collectors.toList());
            commentsInfoVO.setComments(commentAndUserInfoVOList);
            return commentsInfoVO;
        }).collect(Collectors.toList());

        return commentsInfoVOList;
    }


}




