package com.zrl.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zrl.project.constant.RedisConstants;
import com.zrl.project.model.VO.userVO.UserPopoverInfo;
import com.zrl.project.model.entity.*;
import com.zrl.project.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: zrl
 * @date: 2023/4/18 19:11
 * @description:
 */
@Service
public class CommonServiceImpl implements CommonService {

    @Resource
    private UserService userService;

    @Resource
    private BlogService blogService;

    @Resource
    private LikesService likesService;

    @Resource
    private QuestionService questionService;

    @Resource
    private QuestionLikeService questionLikeService;



    @Resource
    private PaperService paperService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    @Override
    @Transactional
    public UserPopoverInfo getUserPopoverInfo(Long id) {

        long likes = 0L;

        //查询用户基本信息
        User user = userService.getById(id);
        UserPopoverInfo userPopoverInfo = new UserPopoverInfo();
        BeanUtils.copyProperties(user,userPopoverInfo);

        //查询粉丝数，和关注数量
        String fansKey = RedisConstants.FANS + id;
        String followedKey = RedisConstants.FOLLOWS + id;
        Set<String> fans = stringRedisTemplate.opsForSet().members(fansKey);
        if (fans==null|| fans.isEmpty()){
            userPopoverInfo.setFansCount(0);
        }else {
            userPopoverInfo.setFansCount(fans.size());
        }

        Set<String> followed = stringRedisTemplate.opsForSet().members(followedKey);
        if (followed == null || followed.isEmpty()){
            userPopoverInfo.setFollowedCount(0);
        }else {
            userPopoverInfo.setFollowedCount(followed.size());
        }

        //查询博客，题目和试卷数量
        LambdaQueryWrapper<Blog> blogLambdaQueryWrapper = new LambdaQueryWrapper<>();
        blogLambdaQueryWrapper.eq(Blog::getUserId,id);
        List<Blog> blogList = blogService.list(blogLambdaQueryWrapper);
        if (blogList == null || blogList.isEmpty()){
            userPopoverInfo.setBlogCount(0L);
        }else{
            List<Long> blogCount = blogList.stream().map(blog -> {
                LambdaQueryWrapper<Likes> blogQueryWrapper = new LambdaQueryWrapper<>();
                blogQueryWrapper.eq(Likes::getBlogId,blog.getId());
                return likesService.count(blogQueryWrapper);
            }).collect(Collectors.toList());
            Long questionLikeCount = blogCount.stream().reduce(Long::sum).orElse(0L);
            likes +=questionLikeCount;
            userPopoverInfo.setBlogCount((long)blogList.size());
        }


        LambdaQueryWrapper<Question> questionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        questionLambdaQueryWrapper.eq(Question::getUserId,id);
        List<Question> list = questionService.list(questionLambdaQueryWrapper);
        if (list == null || list.isEmpty()){
            userPopoverInfo.setQuestionCount(0L);
        }else{
            List<Long> QuestionLikeCount = list.stream().map(question -> {
                LambdaQueryWrapper<QuestionLike> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.eq(QuestionLike::getQuestionId, question.getId());
                long count = questionLikeService.count(queryWrapper);
                return count;
            }).collect(Collectors.toList());
            Long questionLikeCount = QuestionLikeCount.stream().reduce(Long::sum).orElse(0L);
            likes +=questionLikeCount;
            userPopoverInfo.setQuestionCount((long) list.size());
        }

        LambdaQueryWrapper<Paper> paperLambdaQueryWrapper = new LambdaQueryWrapper<>();
        paperLambdaQueryWrapper.eq(Paper::getUserId,id);
        long paperCount = paperService.count(paperLambdaQueryWrapper);
        userPopoverInfo.setPaperCount(paperCount);

        userPopoverInfo.setLikes(likes);

        //查询当前用户是否关注该用户
        Boolean isFollowed = userService.followedUser(id);
        userPopoverInfo.setIsFollowed(isFollowed);
        return userPopoverInfo;
    }
}
