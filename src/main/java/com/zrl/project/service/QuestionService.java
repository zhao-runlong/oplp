package com.zrl.project.service;

import com.zrl.project.model.VO.qustionVO.AddQuestionParamVO;
import com.zrl.project.model.VO.qustionVO.QuestionDetailVO;
import com.zrl.project.model.VO.qustionVO.QuestionWithUserInfoVO;
import com.zrl.project.model.VO.qustionVO.SearchQuestionParamVO;
import com.zrl.project.model.entity.Question;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 13168
* @description 针对表【question(题目表)】的数据库操作Service
* @createDate 2023-04-02 01:22:54
*/
public interface QuestionService extends IService<Question> {

    Boolean addQuestion(AddQuestionParamVO addQuestionParam);

    List<QuestionWithUserInfoVO> queryQuestionsWithUserInfo(SearchQuestionParamVO question);

    QuestionDetailVO getQuestionDetail(Long id);

    boolean updateQuestion(AddQuestionParamVO question);

    boolean deleteQuestion(Long id);

    AddQuestionParamVO queryQuestionById(Long id);

    List<AddQuestionParamVO> queryQuestionWithItemByIds(List<Long> ids);

    List<QuestionWithUserInfoVO> getMyStartQuestion();
}
