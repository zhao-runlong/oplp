package com.zrl.project.service;

import com.zrl.project.model.entity.PaperTags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【paper_tags】的数据库操作Service
* @createDate 2023-04-07 00:45:27
*/
public interface PaperTagsService extends IService<PaperTags> {

}
