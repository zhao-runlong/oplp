package com.zrl.project.service;

import com.zrl.project.model.entity.PaperQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【paper_question】的数据库操作Service
* @createDate 2023-04-06 20:36:24
*/
public interface PaperQuestionService extends IService<PaperQuestion> {

}
