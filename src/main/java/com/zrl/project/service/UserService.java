package com.zrl.project.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zrl.project.model.VO.userVO.ModifyUserPasswordVO;
import com.zrl.project.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 用户服务
 *
 * @author zrl
 */

public interface UserService extends IService<User> {

    /**
     *
     * @param userAccount 用户账号
     * @param userPassword 用户密码
     * @param checkPassword 校验密码
     * @return 新用户id
     */
    long userRegister(String userAccount,String userPassword,String checkPassword);


    /**
     *
     * @param userAccount 用户账号
     * @param userPassword 用户密码
     * @param request
     * @return  当前用户脱敏后返回
     */
    String userLogin(String userAccount, String userPassword, HttpServletRequest request);


    /**
     * 用户脱敏
     * @param user
     * @return 脱敏好的用户
     */
    User getSafetyUser(User user);

    /**
     * 退出登录
     * @param request
     * @return
     */
    int userLogout(HttpServletRequest request);

    /**
     * 添加user
     * @param user
     * @return
     */
    Long addUser(User user);

    /**
     * 条件查询
     * @param userName
     * @param userAccount
     * @param gender
     * @param role
     * @param userStatus
     * @return
     */
    List<User> searchUsers(String userName, String userAccount, Integer gender, Integer role, Integer userStatus);


    /**
     * 修改User
     * @param newUser 修改后的user
     * @return
     */
    boolean updateUserById(User newUser,HttpServletRequest request);

    /**
     * 修改当前用户密码
     * @param user 用户id，旧密码，新密码
     * @return
     */
    boolean modifyUserPassword(ModifyUserPasswordVO user);

    /**
     * 关注用户/取关用户
     * @param userId
     * @return
     */
    Boolean ModifyConcernUser(Long userId);

    /**
     * 查询关注状态
     * @param userId
     * @return
     */
    Boolean followedUser(Long userId);

    /**
     * 查询我的粉丝
     * @return
     */
    List<User> getMyFans();

    /**
     * 查询我的关注
     * @return
     */
    List<User> getMyFollows();

    /**
     * 移除粉丝
     * @param userId
     * @return
     */
    Boolean removeFan(Long userId);

    Boolean concernUser(Long userId);

    Boolean noConcernUser(Long userId);
}
