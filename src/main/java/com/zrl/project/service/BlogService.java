package com.zrl.project.service;

import com.zrl.project.model.VO.blogVO.BlogAndUserInfoVO;
import com.zrl.project.model.VO.blogVO.BlogOrder;
import com.zrl.project.model.VO.blogVO.CommentWithBLogAndUserInfoVo;
import com.zrl.project.model.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zrl.project.model.request.CommentSearchRequest;

import java.util.List;

/**
* @author 13168
* @description 针对表【blog(博客表)】的数据库操作Service
* @createDate 2023-03-22 03:36:59
*/
public interface BlogService extends IService<Blog> {

    /**
     * 查询博客信息和博客作者信息
     * @param id 博客id
     * @return 博客信息和博客作者信息
     */
    BlogAndUserInfoVO getBlogAndUserInfo(Long id);

    /**
     * 查询所有博客
     * @param blog 查询条件
     * @return 所有博客信息
     */
    List<BlogAndUserInfoVO> searchBlogs(BlogOrder blog);

    /**
     * 删除博客
     * @param id 博客id
     * @return 是否删除成功
     */
    boolean deleteBlog(Long id);

    List<CommentWithBLogAndUserInfoVo> queryAllComments(CommentSearchRequest commentSearchRequest);

    List<BlogAndUserInfoVO> getMyStartBlog();
}
