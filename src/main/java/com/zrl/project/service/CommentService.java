package com.zrl.project.service;

import com.zrl.project.model.VO.commentVO.CommentsInfoVO;
import com.zrl.project.model.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 13168
* @description 针对表【comment(评论信息表)】的数据库操作Service
* @createDate 2023-03-26 00:01:21
*/
public interface CommentService extends IService<Comment> {

    /**
     * 根据博客id获取博客所有的评论
     * @param id
     * @return 评论信息
     */
    List<CommentsInfoVO> getCommentsByBlogId(Long id);

    /**
     * 直接对博客评论时修改该评论的顶层评论为自己
     * @param id
     * @return 操作是否成功
     */
    Boolean modifyTopReplyCommentId(Long id);


    /**
     * 删除评论
     * @param id 评论id
     * @return 删除是否成功
     */
    Boolean deleteCommentById(Long id);


    /**
     * 根据题目id获取该题目所有的评论
     * @param id
     * @return 评论信息
     */
    List<CommentsInfoVO> getCommentsByQuestionId(Long id);
}
