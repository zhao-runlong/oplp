package com.zrl.project.service;

import com.zrl.project.model.entity.Tags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【tags(标签表)】的数据库操作Service
* @createDate 2023-03-31 13:35:39
*/
public interface TagsService extends IService<Tags> {

}
