package com.zrl.project.service;

import com.zrl.project.model.VO.userVO.UserPopoverInfo;

/**
 * @author: zrl
 * @date: 2023/4/18 19:11
 * @description:
 */
public interface CommonService {

    UserPopoverInfo getUserPopoverInfo(Long id);

}
