package com.zrl.project.service;

import com.zrl.project.model.entity.QuestionAnswer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【question_answer(题目答案表)】的数据库操作Service
* @createDate 2023-04-01 16:31:56
*/
public interface QuestionAnswerService extends IService<QuestionAnswer> {

}
