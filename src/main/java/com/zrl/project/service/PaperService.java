package com.zrl.project.service;

import com.zrl.project.model.VO.paperVO.AddPaperParamVO;
import com.zrl.project.model.VO.paperVO.PaperDetailVO;
import com.zrl.project.model.VO.paperVO.PaperWithUserInfo;
import com.zrl.project.model.entity.Paper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zrl.project.model.entity.Question;

import java.util.List;

/**
* @author 13168
* @description 针对表【paper(试卷表)】的数据库操作Service
* @createDate 2023-04-07 00:45:31
*/
public interface PaperService extends IService<Paper> {

    Boolean addPaper(AddPaperParamVO addPaperParamVO);

    List<PaperWithUserInfo> queryPapers(AddPaperParamVO paper);

    Boolean deleteById(Integer id);

    PaperDetailVO queryPaperDetail(Integer paperId);

    AddPaperParamVO getUpdatePaper(Integer paperId);

    Boolean updatePaper(AddPaperParamVO addPaperParamVO);
}
