package com.zrl.project.service;

import com.zrl.project.model.VO.messageVO.MessageAndUserInfoVO;
import com.zrl.project.model.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 13168
* @description 针对表【message(消息表)】的数据库操作Service
* @createDate 2023-04-22 23:04:15
*/
public interface MessageService extends IService<Message> {

    List<MessageAndUserInfoVO> getUserMessages();
}
