package com.zrl.project.service;

import com.zrl.project.model.entity.QuestionTags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【question_tags(题目标签表)】的数据库操作Service
* @createDate 2023-03-31 13:41:36
*/
public interface QuestionTagsService extends IService<QuestionTags> {

}
