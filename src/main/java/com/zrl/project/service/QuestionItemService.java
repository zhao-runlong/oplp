package com.zrl.project.service;

import com.zrl.project.model.entity.QuestionItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 13168
* @description 针对表【question_item(题目选项表)】的数据库操作Service
* @createDate 2023-04-01 16:32:00
*/
public interface QuestionItemService extends IService<QuestionItem> {

}
