package com.zrl.project.service;

import com.zrl.project.model.entity.Likes;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zrl.project.model.entity.User;

import java.util.List;

/**
* @author 13168
* @description 针对表【likes(用户喜欢以及博客点赞表)】的数据库操作Service
* @createDate 2023-03-28 19:30:37
*/
public interface LikesService extends IService<Likes> {


    /**
     * 修改点赞状态
     * @param likes 用户id 和 博客id
     * @return 给前端做判断
     */
    Integer modifyLikeStatus(Likes likes);


    /**
     * 获取当前用户是否点过赞
     * @param likes  用户id 和 博客id
     * @return 是否点过赞
     */
    Boolean getLikeStatus(Likes likes);

    /**
     * 获取喜欢该文章的用户信息
     * @param id 博客id
     * @return 所有喜欢该文章的用户信息
     */
    List<User> getLikeUsers(Long id);
}
