package com.zrl.project.common;

import com.zrl.project.constant.UserConstant;
import com.zrl.project.model.entity.User;

import javax.servlet.http.HttpServletRequest;

import static com.zrl.project.constant.UserConstant.ADMIN_ROLE;

/**
 * @author: zrl
 * @date: 2023/3/30 16:28
 * @description:
 */
public class Admin {
    public static boolean isAdmin() {

        User user = UserHolder.getUser();
        if (user == null || user.getRole() != ADMIN_ROLE){
            return false;
        }
        return true;
    }
}
