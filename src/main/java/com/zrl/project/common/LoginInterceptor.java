package com.zrl.project.common;

import com.zrl.project.exception.BusinessException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: zrl
 * @date: 2022/11/9 20:45
 * @description:
 */

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断是否需要拦截,(ThreadLocal中是否有用户)
        if (UserHolder.getUser()==null){
            throw new BusinessException(ErrorCode.NOT_LOGIN_ERROR);
        }
        return true;
    }

}
