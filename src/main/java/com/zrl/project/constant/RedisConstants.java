package com.zrl.project.constant;

/**
 * @author: zrl
 * @date: 2023/4/14 20:59
 * @description:
 */
public interface RedisConstants {


    String LOGIN_USER_KEY = "Login:Token:";


    Long LOGIN_USER_TTL = 3600L;

    String FANS = "fans:";
    String FOLLOWS = "follows:";
}
