package com.zrl.project.constant;

/**
 * 通用常量
 *
 * @author zrl
 */
public interface CommonConstant {

    /**
     * 升序
     */
    String SORT_ORDER_ASC = "ascend";

    /**
     * 降序
     */
    String SORT_ORDER_DESC = " descend";

    /**
     * 最新
     */
    String LATEST = "latest";

    /**
     *最热
     */
    String HOT = "hot";

    /**
     * 精选
     */
    String GOOD = "good";

    /**
     * 优先级
     */
    Integer TOP = 999;
}
