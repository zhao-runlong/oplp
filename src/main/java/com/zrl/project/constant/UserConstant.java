package com.zrl.project.constant;

/**
 * 用户常量
 *
 * @author zrl
 */
public interface UserConstant {

    /**
     * 加密前缀
     */
     String PREFIX = "zrl";

    /**
     * 用户登录态键
     */
    String USER_LOGIN_STATE = "userLoginState";

    /**
     * 系统用户 id（虚拟用户）
     */
    long SYSTEM_USER_ID = 0;

    //  region 权限

    /**
     * 默认权限
     */
    int DEFAULT_ROLE = 0;

    /**
     * 管理员权限
     */
    int ADMIN_ROLE = 1;

    // endregion
}
