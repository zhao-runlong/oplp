package com.zrl.project.mapper;

import com.zrl.project.model.entity.QuestionItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【question_item(题目选项表)】的数据库操作Mapper
* @createDate 2023-04-01 16:32:00
* @Entity com.zrl.project.model.entity.QuestionItem
*/
public interface QuestionItemMapper extends BaseMapper<QuestionItem> {

}




