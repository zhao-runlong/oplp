package com.zrl.project.mapper;

import com.zrl.project.model.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【message(消息表)】的数据库操作Mapper
* @createDate 2023-04-22 23:04:15
* @Entity com.zrl.project.model.entity.Message
*/
public interface MessageMapper extends BaseMapper<Message> {

}




