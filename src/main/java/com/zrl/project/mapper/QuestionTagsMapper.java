package com.zrl.project.mapper;

import com.zrl.project.model.entity.QuestionTags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【question_tags(题目标签表)】的数据库操作Mapper
* @createDate 2023-03-31 13:44:57
* @Entity com.zrl.project.model.entity.QuestionTags
*/
public interface QuestionTagsMapper extends BaseMapper<QuestionTags> {

}




