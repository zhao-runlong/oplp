package com.zrl.project.mapper;

import com.zrl.project.model.entity.PaperTags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【paper_tags】的数据库操作Mapper
* @createDate 2023-04-07 00:45:27
* @Entity com.zrl.project.model.entity.PaperTags
*/
public interface PaperTagsMapper extends BaseMapper<PaperTags> {

}




