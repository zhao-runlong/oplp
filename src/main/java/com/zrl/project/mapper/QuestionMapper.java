package com.zrl.project.mapper;

import com.zrl.project.model.entity.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【question(题目表)】的数据库操作Mapper
* @createDate 2023-04-02 04:40:29
* @Entity com.zrl.project.model.entity.Question
*/
public interface QuestionMapper extends BaseMapper<Question> {

}




