package com.zrl.project.mapper;

import com.zrl.project.model.entity.Paper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【paper(试卷表)】的数据库操作Mapper
* @createDate 2023-04-07 00:45:31
* @Entity com.zrl.project.model.entity.Paper
*/
public interface PaperMapper extends BaseMapper<Paper> {

}




