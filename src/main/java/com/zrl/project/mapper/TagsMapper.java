package com.zrl.project.mapper;

import com.zrl.project.model.entity.Tags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【tags(标签表)】的数据库操作Mapper
* @createDate 2023-03-31 13:35:39
* @Entity com.zrl.project.model.entity.Tags
*/
public interface TagsMapper extends BaseMapper<Tags> {

}




