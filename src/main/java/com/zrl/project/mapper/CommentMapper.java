package com.zrl.project.mapper;

import com.zrl.project.model.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【comment(评论信息表)】的数据库操作Mapper
* @createDate 2023-04-02 02:57:47
* @Entity com.zrl.project.model.entity.Comment
*/
public interface CommentMapper extends BaseMapper<Comment> {

}




