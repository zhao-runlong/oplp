package com.zrl.project.mapper;

import com.zrl.project.model.entity.Likes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【likes(用户喜欢以及博客点赞表)】的数据库操作Mapper
* @createDate 2023-03-28 19:30:37
* @Entity com.zrl.project.model.entity.Likes
*/
public interface LikesMapper extends BaseMapper<Likes> {

}




