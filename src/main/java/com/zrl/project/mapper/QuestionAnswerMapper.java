package com.zrl.project.mapper;

import com.zrl.project.model.entity.QuestionAnswer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【question_answer(题目答案表)】的数据库操作Mapper
* @createDate 2023-04-01 16:31:56
* @Entity com.zrl.project.model.entity.QuestionAnswer
*/
public interface QuestionAnswerMapper extends BaseMapper<QuestionAnswer> {

}




