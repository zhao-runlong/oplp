package com.zrl.project.mapper;

import com.zrl.project.model.entity.PaperQuestion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【paper_question】的数据库操作Mapper
* @createDate 2023-04-06 20:37:19
* @Entity com.zrl.project.model.entity.PaperQuestion
*/
public interface PaperQuestionMapper extends BaseMapper<PaperQuestion> {

}




