package com.zrl.project.mapper;

import com.zrl.project.model.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【blog(博客表)】的数据库操作Mapper
* @createDate 2023-04-16 15:50:03
* @Entity com.zrl.project.model.entity.Blog
*/
public interface BlogMapper extends BaseMapper<Blog> {

}




