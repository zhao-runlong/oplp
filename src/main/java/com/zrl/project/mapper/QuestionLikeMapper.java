package com.zrl.project.mapper;

import com.zrl.project.model.entity.QuestionLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【question_like】的数据库操作Mapper
* @createDate 2023-04-02 02:54:02
* @Entity com.zrl.project.model.entity.QuestionLike
*/
public interface QuestionLikeMapper extends BaseMapper<QuestionLike> {

}




