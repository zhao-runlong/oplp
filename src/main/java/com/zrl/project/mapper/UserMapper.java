package com.zrl.project.mapper;

import com.zrl.project.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13168
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2023-03-30 21:46:57
* @Entity com.zrl.project.model.entity.User
*/
public interface UserMapper extends BaseMapper<User> {

}




