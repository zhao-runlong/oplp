package com.zrl.project.model.request;

import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/3/30 18:01
 * @description:
 */
@Data
public class CommentSearchRequest {

    /**
     * 当前页
     */
    private Integer page;

    /**
     * 每页展示数量
     */
    private Integer pageSize;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 博客Id
     */
    private Long blogId;

    /**
     * 题目Id
     */
    private Long questionId;

    /**
     * 评论内容
     */
    private String content;





}
