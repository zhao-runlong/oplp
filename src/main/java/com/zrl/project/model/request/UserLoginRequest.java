package com.zrl.project.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: zrl
 * @date: 2023/2/24 16:40
 * @description:
 */
@Data
public class UserLoginRequest implements Serializable {


    private String userAccount;

    private String userPassword;

}
