package com.zrl.project.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: zrl
 * @date: 2023/2/24 16:40
 * @description:
 */
@Data
public class UserAddRequest implements Serializable {


    private String userAccount;

    private String userPassword;

    private String userName;

    private String phone;

    private String email;

    private Integer gender;


}
