package com.zrl.project.model.request;

import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: zrl
 * @date: 2023/3/11 16:34
 * @description:
 */
@Data
public class UserSearchRequest implements Serializable {

    private String userAccount;

    private Integer role;

    private Integer userStatus;

    private String userName;

    private Integer gender;

    private Integer page;

    private Integer pageSize;


}
