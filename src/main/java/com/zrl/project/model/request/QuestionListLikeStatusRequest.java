package com.zrl.project.model.request;

import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/11 0:20
 * @description:
 */

@Data
public class QuestionListLikeStatusRequest {
    /**
     * 题目id集合
     */
    private List<Long> questionIds;

    /**
     * 当前用户
     */
    private Long userId;
}
