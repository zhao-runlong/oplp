package com.zrl.project.model.VO.userVO;

import com.zrl.project.model.entity.User;
import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/4/18 19:07
 * @description:
 */
@Data
public class UserPopoverInfo extends User {

    /**
     * 粉丝数
     */
    private Integer fansCount;

    /**
     * 关注数
     */
    private Integer followedCount;

    /**
     * 发布的题目数
     */
    private Long questionCount;

    /**
     * 发布的文章数
     */
    private Long blogCount;

    /**
     * 发布的试卷数
     */
    private Long paperCount;

    /**
     * 被喜欢的总数
     */
    private Long likes;

    /**
     * 是否关注
     */
    private Boolean isFollowed;

}
