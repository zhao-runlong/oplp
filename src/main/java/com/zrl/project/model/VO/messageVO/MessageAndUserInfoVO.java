package com.zrl.project.model.VO.messageVO;

import com.zrl.project.model.entity.Message;
import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/4/22 23:10
 * @description:
 */
@Data
public class MessageAndUserInfoVO extends Message {

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;
}
