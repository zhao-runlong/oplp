package com.zrl.project.model.VO.qustionVO;

import com.zrl.project.model.entity.Question;
import com.zrl.project.model.entity.QuestionItem;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 21:42
 * @description:
 */
@Data
public class QuestionDetailVO extends QuestionWithUserInfoVO {

    /**
     * 题目选项
     */
    private List<QuestionItem> questionItem;

    /**
     * 题目答案
     */
    private List<String> questionAnswer;

}