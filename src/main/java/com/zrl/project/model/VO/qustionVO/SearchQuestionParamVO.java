package com.zrl.project.model.VO.qustionVO;

import com.zrl.project.model.entity.Question;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 21:42
 * @description:
 */
@Data
public class SearchQuestionParamVO extends Question {

    /**
     * 标签
     */
    private List<Integer> tags;

    /**
     * 喜欢数量
     */
    private Long likes;

    /**
     * 评论数量
     */
    private Long commentsCount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;

    /**
     * 分类排序
     */
    private String order;

}
