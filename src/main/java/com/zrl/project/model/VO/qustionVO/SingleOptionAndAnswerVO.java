package com.zrl.project.model.VO.qustionVO;

import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/1 16:36
 * @description:
 */
@Data
public class SingleOptionAndAnswerVO {

    private List<String> options;

    private String answer;

}
