package com.zrl.project.model.VO.userVO;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: zrl
 * @date: 2023/3/17 22:54
 * @description:
 */
@Data
public class ModifyUserPasswordVO implements Serializable {

    private Long id;

    private String userPassword;

    private String newPassword;

    private String checkPassword;
}
