package com.zrl.project.model.VO.qustionVO;

import lombok.Data;

import java.util.List;

/**
 * 多选
 * @author: zrl
 * @date: 2023/4/1 16:36
 * @description:
 */
@Data
public class MultipleOptionAndAnswerVO {

    private List<String> options;

    private List<String> answer;

}
