package com.zrl.project.model.VO.qustionVO;

import com.zrl.project.model.entity.Blog;
import com.zrl.project.model.entity.Question;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 21:42
 * @description:
 */
@Data
public class QuestionWithUserInfoVO extends Question {

    /**
     * 标签
     */
    private List<String> tags;

    /**
     * 喜欢数量
     */
    private Long likes;

    /**
     * 评论数量
     */
    private Long commentsCount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;

}
