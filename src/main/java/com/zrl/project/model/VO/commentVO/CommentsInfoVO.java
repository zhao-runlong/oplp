package com.zrl.project.model.VO.commentVO;

import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 13:11
 * @description:
 */
@Data
public class CommentsInfoVO extends CommentAndUserInfoVO{

    private List<CommentAndUserInfoVO> comments;

}
