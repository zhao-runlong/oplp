package com.zrl.project.model.VO.blogVO;

import com.zrl.project.model.entity.Blog;
import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/4/16 15:31
 * @description:
 */
@Data
public class BlogOrder extends Blog {
    /**
     * 排序方式
     */
    private String order;
}
