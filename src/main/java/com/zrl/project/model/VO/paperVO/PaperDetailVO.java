package com.zrl.project.model.VO.paperVO;

import com.zrl.project.model.VO.qustionVO.QuestionDetailVO;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/10 15:05
 * @description:
 */
@Data
public class PaperDetailVO extends PaperWithUserInfo{
    /**
     * 题目信息
     */
    private List<QuestionDetailVO> questions;
}
