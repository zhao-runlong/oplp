package com.zrl.project.model.VO.paperVO;

import com.zrl.project.model.entity.Paper;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/9 17:53
 * @description:
 */
@Data
public class PaperWithUserInfo extends Paper {

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;

    /**
     * 标签
     */
    private List<String> tags;
}
