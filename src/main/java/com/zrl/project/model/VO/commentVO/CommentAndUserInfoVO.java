package com.zrl.project.model.VO.commentVO;

import com.zrl.project.model.entity.Comment;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 4:40
 * @description:
 */
@Data
public class CommentAndUserInfoVO extends Comment {

    private String userAvatar;

    private String userName;

    private String replyUserName;

}
