package com.zrl.project.model.VO.blogVO;

import com.zrl.project.model.entity.Blog;
import com.zrl.project.model.entity.Comment;
import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/3/30 16:55
 * @description:
 */
@Data
public class CommentWithBLogAndUserInfoVo extends Comment {

    /**
     * 博客名称
     */
    private String blogTitle;

    /**
     * 用户名称
     */
    private String userName;

}
