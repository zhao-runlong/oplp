package com.zrl.project.model.VO.paperVO;

import com.zrl.project.model.entity.Paper;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/9 3:36
 * @description:
 */
@Data
public class AddPaperParamVO extends Paper {

    /**
     *试卷标签
     */
    private List<Integer> tags;

    /**
     *试卷题目
     */
    private List<Long> questionIds;
}
