package com.zrl.project.model.VO.qustionVO;

import com.zrl.project.model.entity.Question;
import lombok.Data;

import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/31 13:46
 * @description:
 */
@Data
public class AddQuestionParamVO extends Question {

    /**
     * 题目标签
     */
    private List<Integer> tags;

    /**
     * 单选
     *
     */
    private SingleOptionAndAnswerVO singleParams;


    /**
     * 多选
     */
    private MultipleOptionAndAnswerVO multipleParams;

}
