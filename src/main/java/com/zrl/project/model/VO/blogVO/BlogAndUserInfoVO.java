package com.zrl.project.model.VO.blogVO;

import com.zrl.project.model.entity.Blog;
import lombok.Data;

/**
 * @author: zrl
 * @date: 2023/3/26 21:42
 * @description:
 */
@Data
public class BlogAndUserInfoVO extends Blog {

    /**
     * 喜欢数量
     */
    private Long likes;

    /**
     * 评论数量
     */
    private Long commentsCount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;

}
