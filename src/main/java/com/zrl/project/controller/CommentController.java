package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.VO.commentVO.CommentsInfoVO;
import com.zrl.project.model.entity.Comment;
import com.zrl.project.service.CommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/26 2:29
 * @description:
 */
@RestController
@RequestMapping("comment")
public class CommentController {

    @Resource
    private CommentService commentService;

    /**
     * 查询博客的所有评论
     * @param id 博客id
     * @return 评论详细信息
     */
    @GetMapping("/getCommentsByBlogId/{id}")
    public BaseResponse<List<CommentsInfoVO>> getCommentsByBlogId(@PathVariable("id") Long id){
        List<CommentsInfoVO> result = commentService.getCommentsByBlogId(id);
        return ResultUtils.success(result);
    }
    /**
     * 查询博客的所有评论
     * @param id 博客id
     * @return 评论详细信息
     */
    @GetMapping("/getCommentsByQuestionId/{id}")
    public BaseResponse<List<CommentsInfoVO>> getCommentsByQuestionId(@PathVariable("id") Long id){
        List<CommentsInfoVO> result = commentService.getCommentsByQuestionId(id);
        return ResultUtils.success(result);
    }

    /**
     * 添加评论
     * @param comment
     * @return
     */
    @PostMapping("/addComment")
    public BaseResponse<Long> addComment(@RequestBody Comment comment){
        if (comment == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean save = commentService.save(comment);
        if (!save){
            throw new BusinessException(ErrorCode.OPERATION_ERROR,"评论失败");
        }
        Long userId = comment.getId();
        return ResultUtils.success(userId);
    }


    @PostMapping("/modifyTopReplyCommentId/{id}")
    public BaseResponse<Boolean> modifyTopReplyCommentId(@PathVariable Long id){
        Boolean result = commentService.modifyTopReplyCommentId(id);
        return ResultUtils.success(result);
    }

    /**
     * 删除评论
     * @param id 评论id
     * @return 是否删除成功
     */
    @DeleteMapping("/deleteComment/{id}")
    public BaseResponse<Boolean> deleteComment(@PathVariable Long id){
        Boolean result = commentService.deleteCommentById(id);
        return ResultUtils.success(result);
    }

}
