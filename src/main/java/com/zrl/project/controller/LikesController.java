package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.entity.Likes;
import com.zrl.project.model.entity.User;
import com.zrl.project.service.LikesService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/28 19:32
 * @description:
 */
@RestController
@RequestMapping("/likes")
public class LikesController {

    @Resource
    private LikesService likesService;


    /**
     * 修改喜欢
     * 如果存在则删除
     * 不存在则添加
     * @param likes
     * @return
     */
    @PostMapping("/modifyLikeStatus")
    public BaseResponse<Integer> modifyLikeStatus(@RequestBody Likes likes){
        if (likes == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
        }
        Integer result = likesService.modifyLikeStatus(likes);
        return ResultUtils.success(result);
    }

    @PostMapping("/getLikeStatus")
    public BaseResponse<Boolean> getLikeStatus(@RequestBody Likes likes){
        if (likes == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
        }
        Boolean result = likesService.getLikeStatus(likes);
        return ResultUtils.success(result);
    }

    @PostMapping("/getLikeUsers/{id}")
    public BaseResponse<List<User>> getLikeUsers(@PathVariable Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"查询点赞用户头像失败");
        }
        List<User> result = likesService.getLikeUsers(id);

        return ResultUtils.success(result);
    }

}
