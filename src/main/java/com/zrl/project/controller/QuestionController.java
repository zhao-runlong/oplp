package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.VO.blogVO.BlogAndUserInfoVO;
import com.zrl.project.model.VO.qustionVO.AddQuestionParamVO;
import com.zrl.project.model.VO.qustionVO.QuestionDetailVO;
import com.zrl.project.model.VO.qustionVO.QuestionWithUserInfoVO;
import com.zrl.project.model.VO.qustionVO.SearchQuestionParamVO;
import com.zrl.project.model.entity.Blog;
import com.zrl.project.model.entity.Question;
import com.zrl.project.service.QuestionService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 单选和判断
 * @author: zrl
 * @date: 2023/3/31 13:48
 * @description:
 */
@RestController
@RequestMapping("/question")
public class QuestionController {

    @Resource
    private QuestionService questionService;

    @PostMapping("/addQuestion")
    public BaseResponse<Boolean> addQuestion(@RequestBody AddQuestionParamVO addQuestionParam){
        if (addQuestionParam == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"题目信息不能为空");
        }
        Boolean flag = questionService.addQuestion(addQuestionParam);
        return ResultUtils.success(flag);
    }

    @PostMapping("/queryQuestionsWithUserInfo")
    public BaseResponse<List<QuestionWithUserInfoVO>> queryQuestionsWithUserInfo(@RequestBody(required = false) SearchQuestionParamVO question){

        List<QuestionWithUserInfoVO> result = questionService.queryQuestionsWithUserInfo(question);
        return ResultUtils.success(result);
    }


    /**
     * 查询题目详情包含评论数据
     * @param id 题目id
     * @return 题目和上传题目用户信息
     */
    @GetMapping("/showQuestionDetailById")
    public BaseResponse<QuestionDetailVO> showBlogDetailById(@RequestParam("id") Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        QuestionDetailVO result = questionService.getQuestionDetail(id);
        return ResultUtils.success(result);
    }

    /**
     * 查询某个题目详情，不包含用户信息和评论数据，只作为更新题目时回显
     * @param id 题目id
     * @return 题目和上传题目用户信息
     */
    @GetMapping("/queryQuestionById")
    public BaseResponse<AddQuestionParamVO> queryQuestionById(@RequestParam("id") Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        AddQuestionParamVO result = questionService.queryQuestionById(id);
        return ResultUtils.success(result);
    }

    /**
     * 试卷选题时，展示已选题目
     * @param ids 题目id集合
     * @return 题目信息
     */
    @PostMapping("/queryQuestionByIds")
    public BaseResponse<List<Question>> queryQuestionByIds(@RequestBody List<Long> ids){
        if (ids == null || ids.isEmpty()){
            ArrayList<Question> questions = new ArrayList<>();
            return ResultUtils.success(questions);
        }
        List<Question> result = questionService.listByIds(ids);
        return ResultUtils.success(result);
    }

    /**
     * 试卷选题时，展示试卷题目合计
     * @param ids 题目id集合
     * @return 题目信息
     */
    @PostMapping("/queryQuestionWithItemByIds")
    public BaseResponse<List<AddQuestionParamVO>> queryQuestionWithItemByIds(@RequestBody List<Long> ids){
        if (ids == null || ids.isEmpty()){
            ArrayList<AddQuestionParamVO> questions = new ArrayList<>();
            return ResultUtils.success(questions);
        }
        List<AddQuestionParamVO> result = questionService.queryQuestionWithItemByIds(ids);
        return ResultUtils.success(result);
    }

    /**
     * 我喜欢的题目
     * @return 题目信息
     */
    @GetMapping("/getMyStartQuestion")
    public BaseResponse<List<QuestionWithUserInfoVO>> getMyStartQuestion(){
        List<QuestionWithUserInfoVO> result = questionService.getMyStartQuestion();
        return ResultUtils.success(result);
    }



    /**
     * 修改题目信息
     * @param question 题目信息表
     * @return 是否修改成功
     */
    @PostMapping("/updateQuestion")
    public BaseResponse<Boolean> updateBlog(@RequestBody AddQuestionParamVO question){
        if (question==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"题目数据为空");
        }
        boolean res = questionService.updateQuestion(question);
        return ResultUtils.success(res);
    }

    /**
     * 删除题目
     * @param id 题目id
     * @return 是否删除成功
     */
    @DeleteMapping("/deleteQuestion/{id}")
    public BaseResponse<Boolean> updateQuestion(@PathVariable Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean res = questionService.deleteQuestion(id);
        return ResultUtils.success(res);
    }

    @PostMapping("/checkSimilarQuestion")
    public BaseResponse<Boolean> checkSimilarQuestion(@RequestBody String title){
        if (title == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Question one = questionService.lambdaQuery().eq(Question::getTitle, title).one();

        return ResultUtils.success(one == null);
    }

}
