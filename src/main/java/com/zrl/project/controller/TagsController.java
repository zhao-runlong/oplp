package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.model.entity.Tags;
import com.zrl.project.service.TagsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/31 12:57
 * @description:
 */
@RestController
@RequestMapping("/tags")
public class TagsController {

    @Resource
    private TagsService tagsService;


    @GetMapping("queryAllTags")
    public BaseResponse<List<Tags>> queryAllTags(){
        List<Tags> list = tagsService.list();
        return ResultUtils.success(list);
    }
}
