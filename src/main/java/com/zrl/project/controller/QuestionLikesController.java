package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.entity.Likes;
import com.zrl.project.model.entity.QuestionLike;
import com.zrl.project.model.entity.User;
import com.zrl.project.model.request.QuestionListLikeStatusRequest;
import com.zrl.project.service.QuestionLikeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/3 18:31
 * @description:
 */
@RestController
@RequestMapping("/questionLike")
public class QuestionLikesController {

    @Resource
    private QuestionLikeService questionLikeService;

    /**
     * 修改喜欢
     * 如果存在则删除
     * 不存在则添加
     * @param questionLike
     * @return
     */
    @PostMapping("/modifyQuestionLikeStatus")
    public BaseResponse<Integer> modifyLikeStatus(@RequestBody QuestionLike questionLike){
        if (questionLike == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
        }
        Integer result = questionLikeService.modifyLikeStatus(questionLike);
        return ResultUtils.success(result);
    }

    @PostMapping("/getQuestionLikeStatus")
    public BaseResponse<Boolean> getQuestionLikeStatus(@RequestBody QuestionLike questionLike){
        if (questionLike == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
        }
        Boolean result = questionLikeService.getQuestionLikeStatus(questionLike);
        return ResultUtils.success(result);
    }

    @PostMapping("/getQuestionListLikeStatus")
    public BaseResponse<List<Boolean>> getQuestionListLikeStatus(@RequestBody QuestionListLikeStatusRequest list){
        if (list == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"点赞失败");
        }
        List<Boolean> result = questionLikeService.getQuestionListLikeStatus(list);
        return ResultUtils.success(result);
    }

    @PostMapping("/getLikeQuestionUsers/{id}")
    public BaseResponse<List<User>> getLikeUsers(@PathVariable Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"查询点赞用户头像失败");
        }
        List<User> result = questionLikeService.getLikeQuestionUsers(id);

        return ResultUtils.success(result);
    }
}
