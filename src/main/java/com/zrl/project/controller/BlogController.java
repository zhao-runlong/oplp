package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.common.UserHolder;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.VO.blogVO.BlogAndUserInfoVO;
import com.zrl.project.model.VO.blogVO.BlogOrder;
import com.zrl.project.model.VO.blogVO.CommentWithBLogAndUserInfoVo;
import com.zrl.project.model.entity.Blog;
import com.zrl.project.model.request.CommentSearchRequest;
import com.zrl.project.service.BlogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.zrl.project.common.Admin.isAdmin;

/**
 * @author: zrl
 * @date: 2023/3/21 3:08
 * @description:
 */
@RestController
@RequestMapping("/blog")
public class BlogController {

    @Resource
    private BlogService blogService;

    /**
     * 添加博客
     * @param blog 博客内容
     * @return 是否添加成功
     */
    @PostMapping("/addBlog")
    public BaseResponse<Boolean> addBlog(@RequestBody Blog blog){
        if (blog==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"文章数据为空");
        }
        boolean res = blogService.save(blog);
        return ResultUtils.success(res);
    }


    /**
     * 修改博客信息
     * @param blog 博客信息表
     * @return 是否修改成功
     */
    @PostMapping("/updateBlog")
    public BaseResponse<Boolean> updateBlog(@RequestBody Blog blog){
        if (blog==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"文章数据为空");
        }
        boolean res = blogService.updateById(blog);
        return ResultUtils.success(res);
    }

    /**
     * 删除博客
     * @param id 博客id
     * @return 是否删除成功
     */
    @DeleteMapping("/deleteBlog/{id}")
    public BaseResponse<Boolean> updateBlog(@PathVariable Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean res = blogService.deleteBlog(id);
        return ResultUtils.success(res);
    }

    /**
     * 查询所有博客
     * @param blog 博客信息（筛选条件）
     * @return 符合条件的所有博客
     */
    @PostMapping("/searchBlogs")
    public BaseResponse<List<BlogAndUserInfoVO>> searchBlogs(@RequestBody(required = false) BlogOrder blog){


        List<BlogAndUserInfoVO> result = blogService.searchBlogs(blog);

        return ResultUtils.success(result);
    }

    /**
     * 查询所有博客
     * @param
     * @return 符合条件的所有博客
     */
    @GetMapping("/getMyStartBlog")
    public BaseResponse<List<BlogAndUserInfoVO>> getMyStartBlog(){

        List<BlogAndUserInfoVO> result = blogService.getMyStartBlog();

        return ResultUtils.success(result);
    }

    /**
     * 查询单个博客内容
     * @param id
     * @return
     */
    @GetMapping("/searchBlog")
    public BaseResponse<Blog> searchBlog(@RequestParam("id") Long id){

        if (id==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"查询失败");
        }
        Blog result = blogService.getById(id);

        return ResultUtils.success(result);
    }


    /**
     * 查询博客详情包含作者信息
     * @param id 博客id
     * @return 博客和作者信息
     */
    @GetMapping("/showBlogDetailById")
    public BaseResponse<BlogAndUserInfoVO> showBlogDetailById(@RequestParam("id") Long id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        BlogAndUserInfoVO result = blogService.getBlogAndUserInfo(id);
        return ResultUtils.success(result);
    }

    /**
     * 查询所有评论信息包含博客信息和用户信息
     * 解决依赖问题，
     * @param request
     * @return 评论信息
     */
    @PostMapping("/queryAllComments")
    public BaseResponse<List<CommentWithBLogAndUserInfoVo>> queryAllComments(
            @RequestBody(required = false) CommentSearchRequest commentSearchRequest,
            HttpServletRequest request){

        //仅管理员可查询
        if (!isAdmin()){
            throw new BusinessException(ErrorCode.NO_AUTH_ERROR);
        }

        List<CommentWithBLogAndUserInfoVo> list =
                blogService.queryAllComments(commentSearchRequest);
        return ResultUtils.success(list);
    }


}
