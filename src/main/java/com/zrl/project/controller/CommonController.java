package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.model.VO.userVO.UserPopoverInfo;
import com.zrl.project.service.CommonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 公共接口
 * @author: zrl
 * @date: 2023/4/18 19:05
 * @description:
 */
@RestController
@RequestMapping("/common")
public class CommonController {

    @Resource
    private CommonService commonService;

    @GetMapping("/getUserPopoverInfo/{id}")
    public BaseResponse<UserPopoverInfo> getUserPopoverInfo(@PathVariable Long id){
        UserPopoverInfo res = commonService.getUserPopoverInfo(id);
        return ResultUtils.success(res);
    }
}
