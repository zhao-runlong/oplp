package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.model.VO.messageVO.MessageAndUserInfoVO;
import com.zrl.project.model.entity.Message;
import com.zrl.project.service.MessageService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/4/22 23:09
 * @description:
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Resource
    private MessageService messageService;

    @GetMapping("/getUserMessage")
    public BaseResponse<List<MessageAndUserInfoVO>> getUserMessage(){
        List<MessageAndUserInfoVO> res = messageService.getUserMessages();
        return ResultUtils.success(res);
    }

    @PostMapping("/sendMessage")
    public BaseResponse<Boolean> sendMessage(@RequestBody Message message){
        boolean save = messageService.save(message);
        return ResultUtils.success(save);
    }

    @PutMapping("/readMessage/{id}")
    public BaseResponse<Boolean> readMessage(@PathVariable Long id){
        boolean save = messageService.lambdaUpdate()
                .eq(Message::getId,id)
                .set(Message::getIsRead,1)
                .update();
        return ResultUtils.success(save);
    }

}
