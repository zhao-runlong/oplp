package com.zrl.project.controller;

import com.zrl.project.common.BaseResponse;
import com.zrl.project.common.ErrorCode;
import com.zrl.project.common.ResultUtils;
import com.zrl.project.exception.BusinessException;
import com.zrl.project.model.VO.paperVO.AddPaperParamVO;
import com.zrl.project.model.VO.paperVO.PaperDetailVO;
import com.zrl.project.model.VO.paperVO.PaperWithUserInfo;
import com.zrl.project.model.entity.Paper;
import com.zrl.project.model.entity.Question;
import com.zrl.project.service.PaperService;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: zrl
 * @date: 2023/3/31 13:48
 * @description:
 */
@RestController
@RequestMapping("/paper")
public class PaperController {

    @Resource
    private PaperService paperService;

    @PostMapping("/addPaper")
    public BaseResponse<Boolean> addPaper(@RequestBody AddPaperParamVO AddPaperParamVO){
        if (AddPaperParamVO == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"试卷不能为空");
        }
        Boolean flag = paperService.addPaper(AddPaperParamVO);
        return ResultUtils.success(flag);
    }
    @PostMapping("/updatePaper")
    public BaseResponse<Boolean> updatePaper(@RequestBody AddPaperParamVO AddPaperParamVO){
        if (AddPaperParamVO == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"试卷不能为空");
        }
        Boolean flag = paperService.updatePaper(AddPaperParamVO);
        return ResultUtils.success(flag);
    }

    @DeleteMapping("/deletePaper/{id}")
    public BaseResponse<Boolean> addPaper(@PathVariable("id") Integer id){
        if (id == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"题目信息不能为空");
        }
        Boolean flag = paperService.deleteById(id);
        return ResultUtils.success(flag);
    }

    @PostMapping("/queryPapers")
    public BaseResponse<List<PaperWithUserInfo>> queryPapers(@RequestBody(required = false) AddPaperParamVO paper){

        List<PaperWithUserInfo> result  = paperService.queryPapers(paper);
        return ResultUtils.success(result);
    }

    @GetMapping("/queryPaperDetail")
    public BaseResponse<PaperDetailVO> queryPapers(@RequestParam("id") Integer paperId){
        PaperDetailVO result  = paperService.queryPaperDetail(paperId);
        return ResultUtils.success(result);
    }

    @GetMapping("/getUpdatePaper")
    public BaseResponse<AddPaperParamVO> getUpdatePaper(@RequestParam("id") Integer paperId){
        AddPaperParamVO result  = paperService.getUpdatePaper(paperId);
        return ResultUtils.success(result);
    }

}
